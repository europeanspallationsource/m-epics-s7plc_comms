# PLC Comms

EPICS module to provide framework for communications to/from Siemens S7-1500 PLC.

* This module uses EPICS **s7plc** driver to provide PLC->EPICS comms
* This module uses EPICS **modbus** driver to provide EPICS->PLC comms
* This overcomes the inherent problem of EPICS **s7plc** driver when it overwrites ALL the data even every time any single value needs to be sent to the PLC

* This module DOES NOT contain any records to be read/written from/to PLC, those records are contained an the actual "plc" module that will "require" THIS module
* This module requires **s7plc** and **modbus** modules. This is done using **Makefile**.
* Comms are configured in *cmd* file
